/*
    postResult takes an object and stores it in a nosql db for data aggregation
    Could be improved by creating a js model for this api call as it gets quite messy constructing this object
*/
export const postResult = (data) => {
    return fetch("https://noroff-quiz-backend.herokuapp.com/result/", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json; encoding=UTF-8"
        }
    }).then(response => response.json());
}