/*
    getCategories uses the open trivia api to fetch a catalog of their categories
*/
export const getCategories = () => {
    return fetch("https://opentdb.com/api_category.php").then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch categories";
        }
    }).then((res) => {
        return res.trivia_categories;
    });
}