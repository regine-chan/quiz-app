/*
    Fetches all the questions by a set of configurations decided under the ConfigureQuizView.vue
    Returns a list of questions based on questionVolume, category and difficulty
*/
export const getQuestions = (categoryId, difficulty, questionVolume) => {
    return fetch(`https://opentdb.com/api.php?amount=${questionVolume}&category=${categoryId}&difficulty=${difficulty}`).then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch questions";
        }
    }).then((res) => {
        if (res.response_code == 1) {
            throw "No questions in category selected";
        } else {
            return res.results;
        }

    });
}