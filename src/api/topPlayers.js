/*
    topPlayers fetches the top scores based on a difficulty and amount param
    fetches this by aggregating results posted by postResults.js
    returns an array of results, sorted by highest score to lowest
*/
export const topPlayers = (difficulty, amount) => {
    return fetch(`https://noroff-quiz-backend.herokuapp.com/aggregation/top-performing/amount=${amount}&difficulty=${difficulty}`, {
        method: "GET",
        headers: {
            "Content-type": "application/json"
        }
    }).then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch categories";
        }
    }).then((res) => {
        return res;
    });
}