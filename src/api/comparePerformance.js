/*
    comparePerformance calls a custom api which calculates the amount of scores lower than yours in a difficulty
    it returns a float
*/
export const comparePerformance = (difficulty, performance) => {
    return fetch(`https://noroff-quiz-backend.herokuapp.com/aggregation/my-performance/score=${performance}&difficulty=${difficulty}`, {
        method: "GET",
        headers: {
            "Content-type": "application/json"
        }
    }).then((res) => {
        if (res.ok) {
            return res.json();
        } else {
            throw "Couldn't fetch categories";
        }
    }).then((res) => {
        return res;
    });
}