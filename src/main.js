import Vue from "vue";
import App from "./App.vue";
import VueRouter from 'vue-router';
import MainView from './views/MainView';
import CategoryView from './views/CategoryView';
import QuestionView from './views/QuestionView';
import ResultView from './views/ResultView';
import HighscoresView from './views/HighscoresView';
import ConfigureQuizView from './views/ConfigureQuizView';
import ErrorView from "./views/ErrorView";

Vue.use(VueRouter);

const routes = [{
    path: '/main',
    name: 'main',
    component: MainView
  },
  {
    path: '/categories',
    name: 'categories',
    component: CategoryView
  },
  {
    path: '/questions/:categoryId&:difficulty&:questionVolume&:username',
    name: 'questions',
    component: QuestionView
  },
  {
    path: '/result',
    name: 'result',
    component: ResultView
  },
  {
    path: '/highscores',
    name: 'highscores',
    component: HighscoresView
  },
  {
    path: '/configure/:categoryId',
    name: 'configureQuiz',
    component: ConfigureQuizView
  }, {
    path: '/error',
    name: 'error',
    component: ErrorView
  },
  {
    path: '/',
    redirect: '/main'
  }
];

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router
}).$mount("#app");