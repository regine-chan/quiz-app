# quiz-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Visit webspite to test the app: 
https://regine-chan.gitlab.io/quiz-app/#/main
